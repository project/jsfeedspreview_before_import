/**
 * @file
 * Preview the feeds csv file.
 */

/*
(function ($) {
  'use strict';
  Drupal.behaviors.jsfeedspreview_before_import = {
    attach: function (context) {
      var inputrad;
      $(".feeds-file-upload .description").html('<p style="color:red">Upload JSON File from local system to see the Preview of the JSON File.</p>');
      $(".feeds-file-upload .descriptiontitle").show();
      $("input#edit-feeds-feedsfilefetcher-upload").change(function (e) {
        var ext = $("input#edit-feeds-feedsfilefetcher-upload").val().split(".").pop().toLowerCase();
        if ($.inArray(ext, ["json"]) !== -1) {
          if (e.target.files !== 'undefined') {
            var reader = new FileReader();
            reader.onload = function (e) {
              var csvval = e.target.result.split("\n");
              var totalrows = csvval.length - 1;
              inputrad = "<div class=previewtitle><span class=lefttitle>preview</span><span class=righttitle>Total number of records :" + "<span class=countrecords>" + totalrows + "</span></span></div>";
              inputrad = inputrad + "<table class=previewtable>";
              for (var k = 0; k < 10; k++) {
                inputrad = inputrad + "<tr class=previewrow>";
                var csvvalue = csvval[k].split(",");
                for (var i = 0; i < csvvalue.length; i++) {
                  var temp = csvvalue[i];
                  inputrad = inputrad + "<td class=previewcolumn>" + temp + "</td>";
                }
                inputrad = inputrad + '</tr>';
                $(".feeds-file-upload .description").html(inputrad);
                $(".feeds-file-upload .descriptiontitle").show();
              }
              inputrad = "</table>";
            };
            reader.readAsText(e.target.files.item(0));
            $(".description").css("width", "auto");
          }
          return false;
        }
      });
    }
  };
})($);
*/
(function ($) {
  'use strict';
  Drupal.behaviors.jsfeedspreview_before_import = {
    attach: function (context) {
      var inputrad;
      $(".feeds-file-upload .description").html('<p style="color:red">Upload JSON File from local system to see the Preview of the JSON File.</p>');
      $(".feeds-file-upload .descriptiontitle").show();
      $("input#edit-feeds-feedsfilefetcher-upload").change(function (e) {
        var ext = $("input#edit-feeds-feedsfilefetcher-upload").val().split(".").pop().toLowerCase();
        if ($.inArray(ext, ["json"]) !== -1) {
          if (e.target.files !== 'undefined') {
            var reader = new FileReader();
            reader.onload = function (e) {
              var fileContent = e.target.result;
              $.jsontotable(fileContent, { id: "#jsontotable-obj", header: false });
              $(".feeds-file-upload .description").html(inputrad);
              $(".feeds-file-upload .descriptiontitle").show();
            };
            reader.readAsText(e.target.files.item(0));
            $(".description").css("width", "auto");
          }
          return false;
        }
      });
    }
  };
})(jQuery);
